﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DRAWebsite.Models
{
    public class ForgotViewModel
    {
        public string Email { get; set; }
    }
}