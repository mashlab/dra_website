﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DRAWebsite.Models
{
    public class Product
    {
        public int Id { get; set; }
        public List<int> FilterIds { get; set; }
    }
}