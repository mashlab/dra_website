﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DRAWebsite.Models
{
    public class ResetViewModel
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
        public string Action { get; set; }
        public bool ResetString { get; set; }
    }
}