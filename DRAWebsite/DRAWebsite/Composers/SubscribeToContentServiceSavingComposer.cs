﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using Umbraco.Core;
using Umbraco.Core.Composing;
using Umbraco.Core.Events;
using Umbraco.Core.Models;
using Umbraco.Core.Services;
using Umbraco.Core.Logging;
using Umbraco.Core.Services.Implement;
using System.Net.Mail;
using Umbraco.Web;
using Umbraco.Web.Mvc;
using System.Web.Security;
using Umbraco.Core.Composing.CompositionExtensions;
using System.Security.Cryptography;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using Microsoft.AspNet.Identity;
using System.Text;

namespace DRAWebsite.Composers
{
    public class SubscribeToContentServiceSavingComposer : IUserComposer
    {
        public void Compose(Composition composition)
        {
            // Append our component to the collection of Components
            // It will be the last one to be run
            composition.Components().Append<SubscribeToContentServiceSavingComponent>();
        }
    }

    public class SubscribeToContentServiceSavingComponent : IComponent
    {
        private readonly ILogger _logger;

        public SubscribeToContentServiceSavingComponent(ILogger logger)
        {
            _logger = logger;
        }
        // initialize: runs once when Umbraco starts
        public void Initialize()
        {
            ContentService.Saving += ContentService_Saving;
            MemberService.Saving += MemberService_Saving;
            MemberService.Saved += MemberService_Saved;
        }

        // terminate: runs once when Umbraco stops
        public void Terminate()
        {
            //unsubscribe during shutdown
            ContentService.Saving -= ContentService_Saving;
            MemberService.Saving -= MemberService_Saving;
            MemberService.Saved -= MemberService_Saved;
        }

        private void ContentService_Saving(IContentService sender, ContentSavingEventArgs e)
        {
            foreach (var content in e.SavedEntities
                // Check if the content item type has a specific alias
                .Where(c => c.ContentType.Alias.InvariantEquals("Member")))
            {
                // Do something if the content is using the MyContentType doctype
                var investor = (Member)content; // Conver the content to a member
                if (investor.Groups.Any() && investor.Groups.Contains("Investors"))
                {
                    Console.WriteLine("Hit this breakpoint please when we save a member."); // Only hit this line of code when we have a investor that has group called investor
                }
            }
        }

        private void MemberService_Saving(IMemberService sender, SaveEventArgs<IMember> e)
        {
            foreach (IMember member in e.SavedEntities)
            {
                // Now we distinguish between new members and previous members.
                if (member.Id <= 0)
                {
                    Console.WriteLine("Hit this breakpoint on new member save please.");
                    // Only now do we build the email to send.
                    try
                    {
                        // We need to get the set Username and password
                        var name = member.Name;
                        var un = member.Username;
                        var email = member.Email;
                        var pw = member.RawPasswordValue;

                        var sm = SendMail(pw, un, email, name);   // Now we just send the mail with the details
                    }
                    catch (Exception ex)
                    {
                        // Basically do nothing, just catch it.
                        Debug.WriteLine(ex);
                    }
                }
            }
        }

        private void MemberService_Saved(IMemberService sender, SaveEventArgs<IMember> e)
        {
            Dictionary<string, string> keyValuePairs = new Dictionary<string, string>();
            int count = 0;
            foreach (IMember member in e.SavedEntities)
            {
                count++;
                // Now we distinguish between new members and previous members.
                Console.WriteLine("Hit this breakpoint on new member save please.");
                // Only now do we build the email to send.
                if (count == 1)
                {
                    try
                {
                    // We need to get the set Username and password
                    var name = member.Name;
                    var un = member.Username;
                    var email = member.Email;
                    var pw = member.RawPasswordValue;
                    // We are going to override the password and create a new one :-), then we can send this one to the new user that has been created.
                    //var newPw = Membership.GeneratePassword(10, 4);
                    //sender.SavePassword(member, newPw);
                    //IMemberService _memberService = Current.Factory.GetInstance<IMemberService>();
                    //_memberService.SavePassword(member, newPw);
                    //var sm = SendMail(pw, un, email, name);   // Now we just send the mail with the details
                    //Debug.WriteLine(sm);
                }
                catch (Exception ex)
                {
                    // Basically do nothing, just catch it.
                    Debug.WriteLine(ex);
                }
                }
            }
        }

        private bool SendMail(string password, string username, string email, string name)
        {
            try
            {
                _logger.Info<SubscribeToContentServiceSavingComponent>($"Sending Mail to newly created member with Email => {email}.");
                var helper = Umbraco.Web.Composing.Current.UmbracoHelper;

                // This is where we go and we retrieve all the information from the Website Configuration
                var wc = helper.ContentAtRoot().FirstOrDefault(x => x.ContentType.Alias == "websiteConfiguration");
                // Find the child
                var investorMailSettings = wc.Children.Where(x => x.ContentType.Alias == "investorPortal").FirstOrDefault();
                if (investorMailSettings == null)
                    investorMailSettings = wc.Children.Where(x => x.ContentType.Alias == "investorPortalMailSettings").FirstOrDefault();

                // If the alias does not exist, create docType
                string smtp = "";
                string from = "";
                string to = email;
                string subject = "";
                string body = "";
                string port = "";
                string cusername = "";
                string cpassword = "";
                bool enablessl = true;
                string loginUrl = "";
                string loginButtonImageUrl = "";
                string draHeaderImageUrl = "";
                if (investorMailSettings != null)
                {
                    _logger.Info<SubscribeToContentServiceSavingComponent>($"Extracting Investor Mail Settings.");

                    smtp = investorMailSettings.HasValue("smtp") ? investorMailSettings.Value<string>("smtp") : "";
                    from = investorMailSettings.HasValue("sendMailFrom") ? investorMailSettings.Value<string>("sendMailFrom") : "";
                    subject = investorMailSettings.HasValue("investorCreationSubject") ? investorMailSettings.Value<string>("investorCreationSubject") : "";
                    port = investorMailSettings.HasValue("smtpPort") ? investorMailSettings.Value<string>("smtpPort") : "";
                    enablessl = investorMailSettings.HasValue("enableSsl") ? investorMailSettings.Value<bool>("enableSsl") : true;
                    cusername = investorMailSettings.HasValue("credentialsUsername") ? investorMailSettings.Value<string>("credentialsUsername") : "";
                    cpassword = investorMailSettings.HasValue("credentialsPassword") ? investorMailSettings.Value<string>("credentialsPassword") : "";
                    body = investorMailSettings.HasValue("investorCreationEmailBody") ? investorMailSettings.Value<string>("investorCreationEmailBody") : "";
                    loginUrl = investorMailSettings.HasValue("investorCreationLoginURL") ? investorMailSettings.Value<string>("investorCreationLoginURL") : "";
                    loginButtonImageUrl = investorMailSettings.HasValue("investorLoginImageButtonURL") ? investorMailSettings.Value<string>("investorLoginImageButtonURL") : "";
                    draHeaderImageUrl = investorMailSettings.HasValue("dRAMailHeaderURL") ? investorMailSettings.Value<string>("dRAMailHeaderURL") : "";
                    //var links = investorMailSettings.Value<IEnumerable<Link>>("resetPasswordButtonURL");
                    //if (links != null && links.Any())
                    //    resetUrl = links.FirstOrDefault().Url;

                    // Get the images 
                    //var resetMediaFile = Current.Services.MediaService.GetById(12098);
                    //resetUrlImage = resetMediaFile.GetValue("umbracoFile").ToString();
                    //var draHeaderImageFile = Current.Services.MediaService.GetById(12096);
                    //draHeaderImageUrl = draHeaderImageFile.GetValue("umbracoFile").ToString();
                }
                else
                {
                    _logger.Warn<SubscribeToContentServiceSavingComponent>($"No Investor Mail Settings Node Found.");
                    return false;
                }


                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient(smtp);

                mail.From = new MailAddress(from);
                mail.To.Add(to);
                mail.Subject = subject;

                // Format body
                body = body.Replace("{user}", name);
                body = body.Replace("{username}", username);
                body = body.Replace("{password}", password);
                body = body.Replace("/{loginUrl}", loginUrl);
                body = body.Replace("/{loginButtonImageUrl}", loginButtonImageUrl);
                body = body.Replace("/{draHeaderImage}", draHeaderImageUrl);
                mail.Body = $"{body}";
                mail.IsBodyHtml = true;

                SmtpServer.Port = Convert.ToInt32(port);
                if (enablessl != false)
                {
                    _logger.Info<SubscribeToContentServiceSavingComponent>($"Mail Settings Enable SSL is set to TRUE.");
                    SmtpServer.Credentials = new System.Net.NetworkCredential(cusername, cpassword);
                    SmtpServer.EnableSsl = enablessl;
                }

                if (from.Contains("@draglobal.com") || from.Contains("@mashlab.co.za"))
                {
                    _logger.Info<SubscribeToContentServiceSavingComponent>($"From Email Contains @draglobal.com or @mashlab.co.za");
                    _logger.Info<SubscribeToContentServiceSavingComponent>($"Now Sending mail to => {email} from => {from}.");
                    SmtpServer.Send(mail);
                }
                else
                {
                    _logger.Warn<SubscribeToContentServiceSavingComponent>($"From Email does not Contain @draglobal.com or @mashlab.co.za");
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                _logger.Error<SubscribeToContentServiceSavingComponent>($"Send Mail to new Member with Email => {email} failed with Exception => {ex.Message}.");
                return false;
            }
        }
    }
}