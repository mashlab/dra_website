﻿function IsValidEmail()
{
    var MemberEmail = $('#email').val();
    var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/;
    var valEmail = pattern.test(MemberEmail);
    if (!valEmail || $('#email').val() === '') {
        $(".email").show();
        return false;

    }
    else {
        $(".email").hide();
        return true;

    }
    
}



function ValidPhoneNum() {
    var PhoneNum = $('#number').val();

    if (PhoneNum === '' || PhoneNum.length > 14 || PhoneNum.length < 5) {

        $(".number").show();
        return false;
    }
    else {

        $(".number").hide();
        return true;
    }

}

function ValidName() {
    //firstname
    if ($('#fullname').val() === '') {

        $(".name").show();
        return false;
    }
    else {

        $(".name").hide();
        return true;
    }
}

function ValidRegion() {
    //firstname
    if ($('#region').val() === '') {

        $(".region").show();
        return false;
    }
    else {

        $(".region").hide();
        return true;
    }
}



function ValidMsg() {
    if ($('#message').val() === '') {

        $(".msg").show();
        return false;
    }
    else {

        $(".msg").hide();
        return true;
    }
}

