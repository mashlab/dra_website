﻿using DRAWebsite.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Runtime.Remoting.Contexts;
using System.Web;
using System.Web.Mvc;
using Umbraco.Core.Models;
using Umbraco.Core.Services.Implement;
using Umbraco.Web;
using Umbraco.Web.Mvc;
using System.Web.Security;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Web.Models;
using Umbraco.Core.Composing;
using Umbraco.Core.Logging;

namespace DRAWebsite.Controllers
{
    public class LoginController : SurfaceController
    {
        [HttpPost]
        public ActionResult ValidateMember(LoginViewModel model)
        {
            if (!String.IsNullOrEmpty(model.Email) && !String.IsNullOrEmpty(model.Password))
            {
                Logger.Info<LoginController>($"Now Validating Member with Email => {model.Email}");
                //if (Members.Login(username, password))
                if (Membership.ValidateUser(model.Email, model.Password))
                {
                    Logger.Info<LoginController>($"User validated with Email => {model.Email}.");
                    FormsAuthentication.SetAuthCookie(model.Email, false);

                    // We need to check if the user is doing this for the first time or not
                    // If they are doing it for the first time we need to redirect them to the reset password page.
                    var member = Services.MemberService.GetByEmail(model.Email);
                    bool loggedInOnce = member.HasProperty("loggedInOnce") ? member.GetValue<bool>("loggedInOnce") : false;
                    if (!loggedInOnce)
                        member.SetValue("loggedInOnce", true);
                    Services.MemberService.Save(member);

                    var hasResetPassword = member.HasProperty("hasResetPassword") ? member.GetValue<bool>("hasResetPassword") : false;
                    var hasUpdatePassword = member.HasProperty("hasUpdatePassword") ? member.GetValue<bool>("hasUpdatePassword") : false;

                    if (!hasUpdatePassword)
                    {
                        Logger.Info<LoginController>($"Member has not updated Password.");
                        return Json(new ResponseDto
                        {
                            Success = true,
                            Message = "update"
                        });
                    }

                    if (!hasResetPassword)
                    {
                        Logger.Info<LoginController>($"Member has not Reset Password");
                        return Json(new ResponseDto
                        {
                            Success = true,
                            Message = "reset"
                        });
                    }

                    //return RedirectToCurrentUmbracoPage();
                    Logger.Info<LoginController>($"Member Validated Successfully with email => {model.Email}.");
                    return Json(new ResponseDto
                    {
                        Success = true,
                        Message = ""
                    });
                }
                else
                {
                    Logger.Error<LoginController>($"Member Validation Failed => Invalid Member.");
                    return Json(new ResponseDto
                    {
                        Success = false,
                        Message = "Invalid Member"
                    });
                }
            }
            else
            {
                Logger.Error<LoginController>($"Member Validation Failed with Invalid Member.");
                return Json(new ResponseDto
                {
                    Success = false,
                    Message = "Invalid Member"
                });
            }
        }

        [HttpPost]
        public ActionResult ResetPassword(ResetViewModel model)
        {
            Logger.Info<LoginController>($"Resetting Password for User with Email => {model.Email}.");
            if (model.Password != model.ConfirmPassword)
            {
                Logger.Warn<LoginController>($"Password Do not match.");
                return Json(new ResponseDto
                {
                    Success = false,
                    Message = "Passwords do not match."
                });
            }
            //var rs = Members.GetByEmail(username).GetProperty("resetstring");
            //if (rs.HasValue && rs.Value == resetstring)
            //{
            //var rp = Members.sa(username, new Umbraco.Web.Models.ChangingPasswordModel { NewPassword = password }, "UmbracoMembershipProvider");
            try
            {
                // Get current logged in user
                var userName = User.Identity.Name;
                var member = Services.MemberService.GetByEmail(userName);
                Services.MemberService.SavePassword(member, model.Password);
                //return RedirectToUmbracoPage(11077);

                // set the updated password
                if (model.Action == "reset")
                {
                    Logger.Info<LoginController>($"Action is Reset, Resetting Password for User with Email => {userName}.");
                    var hasResetPassword = member.HasProperty("hasResetPassword") ? member.GetValue<bool>("hasResetPassword") : false;
                    if (!hasResetPassword)
                        member.SetValue("hasResetPassword", true);
                    Services.MemberService.Save(member);
                }
                else if (model.Action == "update")
                {
                    Logger.Info<LoginController>($"Action is Update, Updating Password for User with Email => {userName}.");
                    var hasUpdatePassword = member.HasProperty("hasUpdatePassword") ? member.GetValue<bool>("hasUpdatePassword") : false;
                    if (!hasUpdatePassword)
                        member.SetValue("hasUpdatePassword", true);
                    var hasResetPassword = member.HasProperty("hasResetPassword") ? member.GetValue<bool>("hasResetPassword") : false;
                    if (!hasResetPassword)
                        member.SetValue("hasResetPassword", true);
                    Services.MemberService.Save(member);
                }

                // Now we sign in with the user and the new password
                if (Membership.ValidateUser(userName, model.Password))
                {
                    Logger.Info<LoginController>($"User Validated Success with Email => {userName}.");
                    return Json(new ResponseDto
                    {
                        Success = true
                    });   // Redirect to investors page
                }
                else
                {
                    Logger.Warn<LoginController>($"User Validation Failed with Email => {userName} with Invalid Member.");
                    return Json(new ResponseDto
                    {
                        Success = false,
                        Message = "Invalid Member"
                    });
                }

            }
            catch (Exception e)
            {
                Logger.Error<LoginController>($"Reset Password failed with Exception => {e.Message}");
                return Json(new ResponseDto
                {
                    Success = false,
                    Message = e.Message
                });
            }
            //}
        }

        [HttpPost]
        public ActionResult ForgotPassword(ForgotViewModel model)
        {
            try
            {
                // Check if member exists
                if (model.Email != null)
                {
                    var member = Services.MemberService.GetByEmail(model.Email);

                    if (member != null)
                    {
                        // Send a reset link to the user.
                        // What we do is we send a temp password to them and then we have them reset the password themeselves upon their first login.
                        // So all we do is basically send them a link to the investors site page

                        // First we change the users password
                        var newPassword = Membership.GeneratePassword(10, 4);

                        Services.MemberService.SavePassword(member, newPassword);
                        member.SetValue("hasResetPassword", false);
                        member.SetValue("loggedInOnce", false);
                        Services.MemberService.Save(member);

                        // Now we send the email
                        if (!SendResetPasswordMail(newPassword, model.Email))
                            throw new Exception("Email Failure");
                    }
                    else
                    {
                        throw new Exception("NoMember");
                    }
                }
                else
                {
                    throw new Exception("No Email to send email to.");
                }

                return Json(new ResponseDto
                {
                    Success = true,
                    Message = ""
                });
            }
            catch (Exception ex)
            {
                return Json(new ResponseDto
                {
                    Success = false,
                    Message = ex.Message
                });
            }
        }

        private bool SendResetPasswordMail(string password, string email)
        {
            try
            {
                Logger.Info<LoginController>($"Sending Reset Password Mail to user with Email => {email}.");
                // This is where we go and we retrieve all the information from the Website Configuration
                var wc = Umbraco.ContentAtRoot().FirstOrDefault(x => x.ContentType.Alias == "websiteConfiguration");
                // Find the child
                var investorMailSettings = wc.Children.Where(x => x.ContentType.Alias == "investorPortal").FirstOrDefault();
                if (investorMailSettings == null)
                    investorMailSettings = wc.Children.Where(x => x.ContentType.Alias == "investorPortalMailSettings").FirstOrDefault();

                // If the alias does not exist, create docType
                string smtp = "";
                string from = "";
                string to = email;
                string subject = "";
                string body = "";
                string port = "";
                string cusername = "";
                string cpassword = "";
                bool enablessl = true;
                string resetUrl = "";
                string resetButtonImageUrl = "";
                string draHeaderImageUrl = "";
                if (investorMailSettings != null)
                {
                    Logger.Info<LoginController>($"Extracting Investor Mail Settings.");

                    smtp = investorMailSettings.HasValue("smtp") ? investorMailSettings.Value<string>("smtp") : "";
                    from = investorMailSettings.HasValue("sendMailFrom") ? investorMailSettings.Value<string>("sendMailFrom") : "";
                    subject = investorMailSettings.HasValue("resetPasswordSubject") ? investorMailSettings.Value<string>("resetPasswordSubject") : "";
                    port = investorMailSettings.HasValue("smtpPort") ? investorMailSettings.Value<string>("smtpPort") : "";
                    enablessl = investorMailSettings.HasValue("enableSsl") ? investorMailSettings.Value<bool>("enableSsl") : true;
                    cusername = investorMailSettings.HasValue("credentialsUsername") ? investorMailSettings.Value<string>("credentialsUsername") : "";
                    cpassword = investorMailSettings.HasValue("credentialsPassword") ? investorMailSettings.Value<string>("credentialsPassword") : "";
                    body = investorMailSettings.HasValue("resetPasswordEmailBody") ? investorMailSettings.Value<string>("resetPasswordEmailBody") : "";
                    resetUrl = investorMailSettings.HasValue("resetPasswordButtonURL") ? investorMailSettings.Value<string>("resetPasswordButtonURL") : "";
                    resetButtonImageUrl = investorMailSettings.HasValue("resetPasswordButtonImageURL") ? investorMailSettings.Value<string>("resetPasswordButtonImageURL") : "";
                    draHeaderImageUrl = investorMailSettings.HasValue("dRAMailHeaderURL") ? investorMailSettings.Value<string>("dRAMailHeaderURL") : "";
                    //var links = investorMailSettings.Value<IEnumerable<Link>>("resetPasswordButtonURL");
                    //if (links != null && links.Any())
                    //    resetUrl = links.FirstOrDefault().Url;

                    // Get the images 
                    //var resetMediaFile = Current.Services.MediaService.GetById(12098);
                    //resetUrlImage = resetMediaFile.GetValue("umbracoFile").ToString();
                    //var draHeaderImageFile = Current.Services.MediaService.GetById(12096);
                    //draHeaderImageUrl = draHeaderImageFile.GetValue("umbracoFile").ToString();
                }
                else
                {
                    Logger.Info<LoginController>($"No Investor Mail Settings Node Found.");
                    return false;
                }


                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient(smtp);

                mail.From = new MailAddress(from);
                mail.To.Add(to);
                mail.Subject = subject;

                // Format body
                body = body.Replace("{password}", password);
                body = body.Replace("/{resetPasswordUrl}", resetUrl);
                body = body.Replace("/{resetPasswordButtonImage}", resetButtonImageUrl);
                body = body.Replace("/{draHeaderImage}", draHeaderImageUrl);
                mail.Body = $"{body}";
                mail.IsBodyHtml = true;

                SmtpServer.Port = Convert.ToInt32(port);
                if (enablessl != false)
                {
                    Logger.Info<LoginController>($"Mail Settings Enable SSL is set to TRUE.");
                    SmtpServer.Credentials = new System.Net.NetworkCredential(cusername, cpassword);
                    SmtpServer.EnableSsl = enablessl;
                }

                if (from.Contains("@draglobal.com") || from.Contains("@mashlab.co.za"))
                {
                    Logger.Info<LoginController>($"From Email Contains @draglobal.com or @mashlab.co.za");
                    Logger.Info<LoginController>($"Now Sending mail to => {email} from => {from}.");
                    SmtpServer.Send(mail);
                }
                else
                {
                    Logger.Warn<LoginController>($"From Email does not Contain @draglobal.com or @mashlab.co.za");
                    return false;
                }

                // If we reach this the mail has been sent
                return true;
            }
            catch (Exception ex)
            {
                Logger.Error<LoginController>($"Send Mail to Member with Email => {email} failed with Exception => {ex.Message}.");
                return false;
            }
        }
    }
}