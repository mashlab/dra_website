﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using Umbraco.Web.Mvc;
using Umbraco.Core.Logging;
using Umbraco.Web;

namespace DRAWebsite.Controllers
{
    public class FormController : SurfaceController
    {
        // GET: Form
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ContactForm(string name, string number, string email, string region, string message, string consent, int node)
        {
            try {
                Logger.Info<FormController>($"Sending Contact Enquiry..");
                // This is where we go and we retrieve all the information from the Website Configuration
                var wc = Umbraco.ContentAtRoot().FirstOrDefault(x => x.ContentType.Alias == "websiteConfiguration");
                // Find the child
                var contactSettings = wc.Children.Where(x => x.ContentType.Alias == "contactSettings").FirstOrDefault();

                string smtp = "";
                string from = "";
                string to = "";
                string subject = "";
                string body = "";
                string port = "";
                string cusername = "";
                string cpassword = "";
                bool enablessl = true;
                if (contactSettings != null)
                {
                    Logger.Info<LoginController>($"Extracting Contact Settings.");

                    smtp = contactSettings.HasValue("smtp") ? contactSettings.Value<string>("smtp") : "";
                    from = contactSettings.HasValue("sendMailFrom") ? contactSettings.Value<string>("sendMailFrom") : "";
                    to = contactSettings.HasValue("sendMailTo") ? contactSettings.Value<string>("sendMailTo") : "";
                    subject = contactSettings.HasValue("resetPasswordSubject") ? contactSettings.Value<string>("resetPasswordSubject") : "";
                    port = contactSettings.HasValue("smtpPort") ? contactSettings.Value<string>("smtpPort") : "";
                    enablessl = contactSettings.HasValue("enableSsl") ? contactSettings.Value<bool>("enableSsl") : true;
                    cusername = contactSettings.HasValue("credentialsUsername") ? contactSettings.Value<string>("credentialsUsername") : "";
                    cpassword = contactSettings.HasValue("credentialsPassword") ? contactSettings.Value<string>("credentialsPassword") : "";
                }

                if (consent == "on")
                    consent = "Yes";
                else
                    consent = "No";

                body = "Fullname:" + " " + name + "<br/>" + "Contact  number:" + " " + number + "<br/>" + "<br/>" + "Region" + " " + region + "<br/>" + "Email:" + " " + email + "<br/><br/>" + "Comment:" + " " + message + "<br/><br/>";
                if (consent == "on")
                {
                    body += "Consent to receiving communication from DRA:" + " " + "<b>Yes</b>";
                }
                else
                {
                    body += "Consent to receiving communication from DRA:" + " " + "<b>No</b>";
                }

                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient(smtp);

                mail.From = new MailAddress(from);
                mail.To.Add(to);
                mail.Subject = subject;
                mail.Body = body;
                mail.IsBodyHtml = true;

                SmtpServer.Port = Convert.ToInt32(port);

                if (enablessl != false)
                {
                    Logger.Info<LoginController>($"Mail Settings Enable SSL is set to TRUE.");
                    SmtpServer.Credentials = new System.Net.NetworkCredential(cusername, cpassword);
                    SmtpServer.EnableSsl = enablessl;
                }

                if (from.Contains("@draglobal.com") || from.Contains("@mashlab.co.za"))
                {
                    Logger.Info<LoginController>($"From Email Contains @draglobal.com or @mashlab.co.za");
                    Logger.Info<LoginController>($"Now Sending mail to => {email} from => {from}.");
                    SmtpServer.Send(mail);
                }
                else
                {
                    Logger.Warn<LoginController>($"From Email does not Contain @draglobal.com or @mashlab.co.za");
                    throw new Exception("From Email Does not Contain Correct Requirements.");
                }

                var result = new { Success = "true" };
                return Json(result, JsonRequestBehavior.AllowGet);
            } catch(Exception ex)
            {
                Logger.Error<FormController>($"Contact Enquiry Failed with Exception => {ex.Message}.");
                var result = new { Success = "false" };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }
    }
}